use iui::controls::{Button, Entry, GridAlignment, GridExpand, Group, Label, LayoutGrid, Window};
use iui::prelude::TextEntry;
use iui::UI;

pub struct PropertyGrid {
    ui: UI,
    window: Window,
    group: Group,
    entries: Vec<Entry>,
}

impl<'a> PropertyGrid {
    pub fn new(mut ui: UI, window: Window, title: &str, labels: &[&str]) -> PropertyGrid {
        let mut group = Group::new(&ui, title);

        let mut grid = LayoutGrid::new(&ui);
        grid.set_padded(&ui, true);

        let mut entries = Vec::with_capacity(labels.len());

        for (i, label) in labels.iter().enumerate() {
            let mut label = Label::new(&ui, label);
            let mut entry = Entry::new(&ui);
            //ui.set_enabled(entry.clone(), false);

            entries.push(entry.clone());

            grid.append(
                &ui,
                label,
                0,
                i as i32,
                1,
                1,
                GridExpand::Neither,
                GridAlignment::Start,
                GridAlignment::Center,
            );
            grid.append(
                &ui,
                entry,
                1,
                i as i32,
                1,
                1,
                GridExpand::Horizontal,
                GridAlignment::Fill,
                GridAlignment::Fill,
            );
        }

        group.set_child(&ui, grid);

        PropertyGrid {
            ui,
            window,
            group,
            entries,
        }
    }

    pub fn get_group(&self) -> &Group {
        &self.group
    }

    pub fn set_values(&mut self, values: &[&str]) {
        if self.entries.len() == values.len() {
            for (i, entry) in self.entries.iter_mut().enumerate() {
                entry.set_value(&self.ui, values[i]);
            }
        } else {
            self.window.modal_err(
                &self.ui,
                "Internal error",
                "set_values parameter must have same length as count of controls",
            );
        }
    }
}
