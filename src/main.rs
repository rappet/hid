#![windows_subsystem = "windows"]

extern crate hidapi;
extern crate iui;
extern crate smartstring;

use hidapi::DeviceInfo;
use iui::controls::{
    Button, Control, Entry, GridAlignment, GridExpand, Group, Label, LayoutGrid, VerticalBox,
};
use iui::prelude::*;
use smartstring::alias::String;

use crate::controls::device_info::PropertyGrid;
use std::convert::TryFrom;
use std::fmt::Write;

mod controls;

fn format_device_info(device: &DeviceInfo) -> String {
    let mut output = String::new();
    write!(
        output,
        "Bus {:03} ID {:04x}:{:04x} {:10} {:10}",
        device.interface_number(),
        device.vendor_id(),
        device.product_id(),
        device.manufacturer_string().unwrap_or("<unknown>"),
        device.product_string().unwrap_or("<unknown>")
    )
    .unwrap();
    output
}

fn main() {
    println!("Hello World!");
    let api = hidapi::HidApi::new().unwrap();

    let mut ui = UI::init().expect("Couldn't initialize UI library");
    let mut win = Window::new(&ui, "Test App", 400, 400, WindowType::NoMenubar);
    win.set_title(&ui, "HID Toolkit");

    let mut vbox = VerticalBox::new(&ui);
    vbox.set_padded(&ui, true);

    let mut device_info = PropertyGrid::new(
        ui.clone(),
        win.clone(),
        "",
        &["Vendor ID/Product ID:", "Product:", "Vendor:", "Interface Nr.:"],
    );

    if let Some(device) = api.device_list().next() {
        device_info.set_values(&[
            &format!("{:04X}/{:04X}", device.vendor_id(), device.product_id()),
            device.product_string().unwrap_or("<missing>"),
            device.manufacturer_string().unwrap_or("<missing>"),
            &format!("{}", device.interface_number()),
        ]);
    }

    vbox.append(
        &ui,
        device_info.get_group().clone(),
        LayoutStrategy::Compact,
    );

    win.set_child(&ui, vbox);

    win.show(&ui);

    ui.main();
}
